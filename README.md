# Advanced Programming Even 2019/2020 Midterm Exam

CSCM602023 - Advanced Programming (Pemrograman Lanjut) @
Faculty of Computer Science Universitas Indonesia, Even Semester 2019/2020
* * *

## Table of Contents

Welcome to the code repository.
This repository hosts weekly tutorial codes and other, such as course-related
code snippets.

1. [Introduction](#introduction)
2. [Rules of Doing Exam](#rules-of-doing-exam)
3. [Code of Conduct](#code-of-conduct)
3. Problem Sets
    1. [Problem Set 1](one) - Creational & Structural Design Patterns
    2. [Problem Set 2](two) - Behavioral Design Patterns
    3. [Problem Set 3](three) - Refactoring with Design Patterns

## Setup Environment
1. Make sure that your system has Java already installed (preferred Java 8).
2. Setup your favorite text editor (Intelij IDEA, Sublime, Scite, Notepad++, etc.)
3. Since this is a native Java Program, make sure you know how to compile and run it. (If you forgot, you can
see the tutorial [here](https://www.tutorialspoint.com/How-to-run-a-java-program))

## Rules of Doing Exam

### Workflow to answer and submit the answer of Problem Set
1. Programming Questions
    * Create a new branch based on which problem set you currently work to implement your changes (e.g. prob-1).
    * Change the codebase to implement your solution.
2. Essay Questions
    * Create a new branch based on which problem set you currently work to implement your changes (e.g. prob-1).
    * You only need to write down your answer by replacing the text `Replace this text with your answer`.
    * You may write your answer using English or Indonesian.

After working on all question, create .txt file that contains your Gitlab Repository URL and the branch you used for solving the problem set (e.g. prob-1). Here's the workflow example:

1. Suppose `Andi` wants to work on this Midterm Problem Sets, first he perform ***Forking*** on this repository.
2. Make sure that the repository as a result of forking is **Public**. You can check it by going to menu `Settings -> Visbility -> Project Visibility`.
3. Working on the problem set based on the deadline set in **Scele submission**. To start working, `Andi`
create a new branch named `ans-prob-n` from `master` branch, where `n` is the problem set number.
    * `Andi` start answering the Essay question. He write the answer by replacing the box
    
    `Replace this text with your answer`
     
     with his answer. For example:
     
     `The correct design pattern for this codebase is Chain of Responsibility Pattern`
     
     * `Andi` start answering the Programming question by change/add/remove lines of code in the codebase. 
4. After finish working all the questions, `Andi` submit a txt file (e.g. `ans-prob-1.txt`) to a designated Scele submimssion. The content of txt file is: 
```
1. Name: Andi Sudjani
2. Gitlab URL: https://gitlab.com/andi.sudjani/midterm-2020-online
3. Branch name: ans-prob-1 
```
He continue to perform step 3 and 4 until all the problem sets are answered.
* * *

### Code of Conduct
1. This exam is ***OPEN ALL*** which means that you can use book, slides, written notes, even use google :)
2. Since this is a *Midterm Exam*, you must ***WRITE YOUR OWN ANSWER***. Any kind of plagiarism or miscoduct of academic ethics will be punished severely according to existing provisions (SK DGB UI No.1 Tahun 2014) with a maximum punishment is getting ***E*** as Final Mark of this class.
3. Read the instruction and description in each Problem sets thoroughly before answering.
4. Submission other than allowed mechanisms will not be accepted.
5. Always check the deadline of each Problem Set. *Only commit that pushed to remote origin before the deadline will be reviewed*. You may submit the txt file according to the Scele Submission deadline.
* * *

Goodluck to you all and stay healthy guys. #DiRumahAja